package com.javamaster.lwjgldemo;

import java.io.IOException;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.util.ResourceLoader;

class TextureLoader {

    static Texture grassTop,grass;

    static void init() {
        try {
            grassTop = org.newdawn.slick.opengl.TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/grassTop.png"));
            grass = org.newdawn.slick.opengl.TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/grass.png"));
        } catch (IOException e) {
            System.err.println(Const.TEXTURE_LOADER_ERROR);
            e.printStackTrace();
            new Main().cleanUp(true);
        }
    }
}