package com.javamaster.lwjgldemo;

import org.lwjgl.opengl.GL11;

public class Cube {

    float xS,yS,zS,xE,yE,zE;
    float width;
    float height;
    float length;
    float centerX;
    float centerY;
    float centerZ;
    float yTop;
    float yBottom;
    public Cube(float xS,float yS,float zS,float xE,float yE,float zE) {
        this.xS = xS;
        this.yS = yS;
        this.zS = zS;
        this.xE = xE;
        this.yE = yE;
        this.zE = zE;
        width = xE - xS;
        height = yE - yS;
        length = zE - zS;
        centerX = xE + (width / 2);
        centerY = yE + (height / 2);
        centerZ = zE + (length / 2);
        yTop = Math.abs(yE - yS);
        yBottom = yS + yTop - height;
    }

    public void render(){
        GL11.glColor3f(0,1,1);
        TextureLoader.wood.bind();

        GL11.glPushMatrix();
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glNormal3f(0.0f, 0.0f, 1.0f);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f (1.0f, 1.0f, 1.0f);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f (-1.0f, 1.0f, 1.0f);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f (-1.0f, -1.0f, 1.0f);
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f (1.0f, -1.0f, 1.0f);
        GL11.glEnd();
        GL11.glPopMatrix();

        GL11.glColor3f(1,0,1);
        GL11.glPushMatrix();
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glNormal3f(0.0f, 0.0f, -1.0f);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f (1.0f, 1.0f, -1.0f);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f (1.0f, -1.0f, -1.0f);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f (-1.0f, -1.0f, -1.0f);
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f (-1.0f, 1.0f, -1.0f);
        GL11.glEnd();
        GL11.glPopMatrix();

        GL11.glColor3f(1,1,0);
        GL11.glPushMatrix();
        GL11.glBegin (GL11.GL_QUADS);
        GL11.glNormal3f(-1.0f, 0.0f, 0.0f);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f (-1.0f, 1.0f, 1.0f);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f (-1.0f, 1.0f, -1.0f);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f (-1.0f, -1.0f, -1.0f);
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f (-1.0f, -1.0f, 1.0f);
        GL11.glEnd();
        GL11.glPopMatrix();

        /*GL11.glPushMatrix();
        GL11.glBegin (GL11.GL_QUADS);
        //GL11.glNormal3f(1.0f, 0.0f, 0.0f);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f (1.0f, 1.0f, 1.0f);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f (1.0f, -1.0f, 1.0f);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f (1.0f, -1.0f, -1.0f);
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f (1.0f, 1.0f, -1.0f);
        GL11.glEnd();
        GL11.glPopMatrix();

        GL11.glPushMatrix();
        GL11.glBegin (GL11.GL_QUADS);
        //GL11.glNormal3f(0.0f, 1.0f, 0.0f);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f (-1.0f, 1.0f, -1.0f);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f (-1.0f, 1.0f, 1.0f);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f (1.0f, 1.0f, 1.0f);
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f (1.0f, 1.0f, -1.0f);
        GL11.glEnd();
        GL11.glPopMatrix();

        GL11.glPushMatrix();
        GL11.glBegin(GL11.GL_QUADS);
        //GL11.glNormal3f(0.0f, -1.0f, 0.0f);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f (-1.0f, -1.0f, -1.0f);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f (1.0f, -1.0f, -1.0f);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f (1.0f, -1.0f, 1.0f);
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f (-1.0f, -1.0f, 1.0f);
        GL11.glEnd();*/
        //GL11.glPopMatrix();
    }
}
