package com.javamaster.lwjgldemo;

class Const {

    //Strings
    static final String TEXTURE_LOADER_ERROR = "TextureLoader error";
    static final String TITLE = "GL3Dgame";
    static final String DISPLAY_CREATE_ERROR = "Display create error";
    //ints
    static final int DISPLAY_HEIGHT = 600;
    static final int DISPLAY_WIDTH = 800;
    static final int MAX_FPS = 100;
}
