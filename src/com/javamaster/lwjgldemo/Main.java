package com.javamaster.lwjgldemo;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public class Main {

    static boolean[] keys = new boolean[256];
    private boolean VSYNC_ENABLED = false;
    private boolean RUNNING = true;
    static float dt = 0.0f;
    private static long lastFPS;
    private static int fps;

    // создание камеры
    private Camera camera = new Camera(this);

    private static long GetTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    private void UpdateFPS() {
        if (GetTime() - lastFPS > 1000) {
            Display.setTitle(Const.TITLE + " FPS: " + fps);
            fps = 0;
            lastFPS += 1000;
        }
        fps++;
    }

    // заготовка для настройки дисплея.
    private void GET_INIT_DISPLAY() {
        try {
            // настраиваем размеры окна
            Display.setDisplayMode(new DisplayMode(Const.DISPLAY_WIDTH, Const.DISPLAY_HEIGHT));
            // меняем заголовок окна
            Display.setTitle(Const.TITLE);
            // вертикальная синхронизация в этот момент отключена
            Display.setVSyncEnabled(VSYNC_ENABLED);
            // создаем окно
            Display.create();
        } catch (LWJGLException e) {
            System.err.println(Const.DISPLAY_CREATE_ERROR);
            e.printStackTrace();
            cleanUp(true);
        }

        // это настройка Opengl
        GET_INIT_GL3D();
        // запускаем цикл
        GET_INIT_RUNNING();
    }

    // заготовка для настройки 3D OpenGL.
    private void GET_INIT_GL3D() {
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        //65.0f, (float) (DISPLAY_WIDTH / DISPLAY_HEIGHT),0.0001f, 1050
        GLU.gluPerspective(90.f, 1.f, 1.f, 2000.f);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        GL11.glClearDepth(1.0f);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthFunc(GL11.GL_LEQUAL);
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }



    // ну а здесь будет главный цикл игры.
    private void GET_INIT_RUNNING() {
        float lastTime = 0.0f;
        float time;
        lastFPS = GetTime();
        TextureLoader.init();
        World();
        // цикл будет работать до тех пор, пока RUNNING не будет равен false;
        // окно не будет закрыто; пользователь не нажмет клавишу ESC.
        while (RUNNING && !Display.isCloseRequested()
                && !keys[Keyboard.KEY_ESCAPE]) {
            time = Sys.getTime();
            dt = (time - lastTime) / 1000.0f;
            lastTime = time;
            GET_INIT_UPDATE();
            // обновление окна
            Display.update();
            Display.sync(Const.MAX_FPS);
        }
        // asCrash == false это число 0. Выход в нормальном состоянии
        cleanUp(false);
    }

    // здесь мы поместим все в один метод, чтобы было легче работать.
    private void GET_INIT_UPDATE() {
        // установка камеры
        camera.Update();
        // скрытие курсора
        Mouse.setGrabbed(true);
        mapKeys();
        GET_INIT_RENDER();
        UpdateFPS();
    }

    private void GET_INIT_RENDER() {
        // Очистить экран и буфер глубины
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glLoadIdentity();
        // установка позиции камеры
        camera.TranslateCamera();
        // рендеринг всех блоков
        for (Quad q : quad) {
            q.render();
        }
    }

    // массив для хранения Keyboard
    private void mapKeys() {
        for (int i = 0; i < keys.length; i++) {
            keys[i] = Keyboard.isKeyDown(i);
        }
    }

    // очистка и разрушение окна
    void cleanUp(boolean asCrash) {
        Display.destroy();
        System.exit(asCrash ? 1 : 0);
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.GET_INIT_DISPLAY();
    }
}
