package com.javamaster.lwjgldemo;

import org.lwjgl.opengl.GL11;

class Quad {

    float x1, y1, z1, x2, y2, z2, centerX, centerY, centerZ;
    float width, height, length;
    float yTop, yBottom;
    int num;
    float cX2, cY2, cZ2;
    float texSize;
    boolean top, bottom, front, back, left_side, right_side;

    public Quad(float startX, float startY, float startZ, float endX,
                float endY, float endZ, int number, float cxx, float cyy, float czz,
                float ts, boolean topB, boolean bottomB, boolean frontB, boolean backB,
                boolean lsB, boolean rsB) {
        x1 = startX;
        y1 = startY;
        z1 = startZ;
        x2 = endX;
        y2 = endY;
        z2 = endZ;
        width = x2 - x1;
        height = y2 - y1;
        length = z2 - z1;
        centerX = x1 + (width / 2);
        centerY = y2 + (height / 2);
        centerZ = z1 + (length / 2);
        yTop = Math.abs(y2 - y1);
        yBottom = y1 + yTop - height;
        num = number;
        cX2 = cxx;
        cY2 = cyy;
        cZ2 = czz;
        texSize = ts;
        top = topB;
        bottom = bottomB;
        front = frontB;
        back = backB;
        left_side = lsB;
        right_side = rsB;
    }

    private void texNum() {
        if (num == 1) {
            TextureLoader.wood.bind();
        }
    }

    void render() {
        GL11.glColor3f(cX2, cY2, cZ2);
        texNum();
        if (top) {
            GL11.glPushMatrix();
            GL11.glBegin(GL11.GL_POLYGON);
            GL11.glTexCoord2f(0, 0);
            GL11.glNormal3f(x1, y1, z1);
            GL11.glVertex3f(x1, y1, z1);
            GL11.glTexCoord2f(texSize, 0);
            GL11.glNormal3f(x1 + width, y1, z1);
            GL11.glVertex3f(x1 + width, y1, z1);
            GL11.glTexCoord2f(texSize, texSize);
            GL11.glNormal3f(x1 + width, y1, z1 + length);
            GL11.glVertex3f(x1 + width, y1, z1 + length);
            GL11.glTexCoord2f(0, texSize);
            GL11.glNormal3f(x1, y1, z1 + length);
            GL11.glVertex3f(x1, y1, z1 + length);
            GL11.glEnd();
            GL11.glPopMatrix();
        }
        // GL11.glColor3f(0.2f, 0.2f, 0.2f);
        if (bottom) {
            GL11.glPushMatrix();
            GL11.glBegin(GL11.GL_POLYGON);
            GL11.glTexCoord2f(0, 0);
            GL11.glNormal3f(x1, y1 + height, z1);
            GL11.glVertex3f(x1, y1 + height, z1);
            GL11.glTexCoord2f(texSize, 0);
            GL11.glNormal3f(x1 + width, y1 + height, z1);
            GL11.glVertex3f(x1 + width, y1 + height, z1);
            GL11.glTexCoord2f(texSize, texSize);
            GL11.glNormal3f(x1 + width, y1 + height, z1 + length);
            GL11.glVertex3f(x1 + width, y1 + height, z1 + length);
            GL11.glTexCoord2f(0, texSize);
            GL11.glNormal3f(x1, y1 + height, z1 + length);
            GL11.glVertex3f(x1, y1 + height, z1 + length);
            GL11.glEnd();
            GL11.glPopMatrix();
        }
        // GL11.glColor3f(0.4f, 0.4f, 0.4f);
        if (front) {
            GL11.glPushMatrix();
            GL11.glBegin(GL11.GL_POLYGON);
            GL11.glTexCoord2f(0, 0);
            GL11.glNormal3f(x1, y1, z1);
            GL11.glVertex3f(x1, y1, z1);
            GL11.glTexCoord2f(texSize, 0);
            GL11.glNormal3f(x1 + width, y1, z1);
            GL11.glVertex3f(x1 + width, y1, z1);
            GL11.glTexCoord2f(texSize, texSize);
            GL11.glNormal3f(x1 + width, y1 + height, z1);
            GL11.glVertex3f(x1 + width, y1 + height, z1);
            GL11.glTexCoord2f(0, texSize);
            GL11.glNormal3f(x1, y1 + height, z1);
            GL11.glVertex3f(x1, y1 + height, z1);
            GL11.glEnd();
            GL11.glPopMatrix();
        }
        // GL11.glColor3f(0.4f, 0.4f, 0.4f);
        if (back) {
            GL11.glPushMatrix();
            GL11.glBegin(GL11.GL_POLYGON);
            GL11.glTexCoord2f(0, 0);
            GL11.glNormal3f(x1, y1, z1 + length);
            GL11.glVertex3f(x1, y1, z1 + length);
            GL11.glTexCoord2f(texSize, 0);
            GL11.glNormal3f(x1 + width, y1, z1 + length);
            GL11.glVertex3f(x1 + width, y1, z1 + length);
            GL11.glTexCoord2f(texSize, texSize);
            GL11.glNormal3f(x1 + width, y1 + height, z1 + length);
            GL11.glVertex3f(x1 + width, y1 + height, z1 + length);
            GL11.glTexCoord2f(0, texSize);
            GL11.glNormal3f(x1, y1 + height, z1 + length);
            GL11.glVertex3f(x1, y1 + height, z1 + length);
            GL11.glEnd();
            GL11.glPopMatrix();
        }
        // GL11.glColor3f(1.0f, 1.0f, 1.0f);
        if (left_side) {
            GL11.glPushMatrix();
            GL11.glBegin(GL11.GL_POLYGON);
            GL11.glTexCoord2f(0, 0);
            GL11.glNormal3f(x1, y1, z1);
            GL11.glVertex3f(x1, y1, z1);
            GL11.glTexCoord2f(texSize, 0);
            GL11.glNormal3f(x1, y1, z1 + length);
            GL11.glVertex3f(x1, y1, z1 + length);
            GL11.glTexCoord2f(texSize, texSize);
            GL11.glNormal3f(x1, y1 + height, z1 + length);
            GL11.glVertex3f(x1, y1 + height, z1 + length);
            GL11.glTexCoord2f(0, texSize);
            GL11.glNormal3f(x1, y1 + height, z1);
            GL11.glVertex3f(x1, y1 + height, z1);
            GL11.glEnd();
            GL11.glPopMatrix();
        }
        // GL11.glColor3f(1.0f, 1.0f, 1.0f);
        if (right_side) {
            GL11.glPushMatrix();
            GL11.glBegin(GL11.GL_POLYGON);
            GL11.glTexCoord2f(0, 0);
            GL11.glNormal3f(x1 + width, y1, z1);
            GL11.glVertex3f(x1 + width, y1, z1);
            GL11.glTexCoord2f(texSize, 0);
            GL11.glNormal3f(x1 + width, y1, z1 + length);
            GL11.glVertex3f(x1 + width, y1, z1 + length);
            GL11.glTexCoord2f(texSize, texSize);
            GL11.glNormal3f(x1 + width, y1 + height, z1 + length);
            GL11.glVertex3f(x1 + width, y1 + height, z1 + length);
            GL11.glTexCoord2f(0, texSize);
            GL11.glNormal3f(x1 + width, y1 + height, z1);
            GL11.glVertex3f(x1 + width, y1 + height, z1);
            GL11.glEnd();
            GL11.glPopMatrix();
        }
        // создание цвета по умолчанию
        GL11.glColor3f(1.0f, 1.0f, 1.0f);
    }
}
