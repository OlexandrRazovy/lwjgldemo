package com.javamaster.lwjgldemo;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

class Camera {

    private boolean moveForward = false;
    private boolean moveBackward = false;
    private boolean strafeLeft = false;
    private boolean strafeRight = false;

    private boolean onGround = false, jumping = false, falling = false;
    private final float gravity = 0.0100f;

    float movementSpeed = 8.0f;
    float yBottom = 0;
    float yMotion = 0;

     Main world;

    static Vector3f vector = new Vector3f();
    Vector3f rotation = new Vector3f();
    Vector3f previous = new Vector3f();

    Camera(Main app) {
        world = app;
    }

    public Camera() {

    }

    public static float getX() {
        return vector.x;
    }

    public static float getY() {
        return vector.y;
    }

    public static float getZ() {
        return vector.z;
    }

    void Update() {
        UpdatePreviousVector();
        UpdateMotion(movementSpeed * Main.dt);
        Input();
        collisions(-0.9f);
        Physics();
    }

    // позиуия камеры
    void TranslateCamera() {
        GL11.glRotatef(rotation.x, 1.0f, 0.0f, 0.0f);
        GL11.glRotatef(rotation.y, 0.0f, 1.0f, 0.0f);
        GL11.glRotatef(rotation.z, 0.0f, 0.0f, 1.0f);
        GL11.glTranslatef(-vector.x, -vector.y - 2.4f, -vector.z);
    }

    private void UpdatePreviousVector() {
        previous.x = vector.x;
        previous.y = vector.y;
        previous.z = vector.z;
    }

    // физика камеры
    private void Physics() {
        onGround = (vector.y == yBottom && !jumping && !falling);
        jumping = (yMotion > 0);
        falling = (yMotion < 0);
        vector.y += yMotion;
        if (vector.y > yBottom) {
            yMotion -= gravity;
        }
        if (vector.y <= yBottom) {
            vector.y = yBottom;
            yMotion = 0;
        }
    }

    private void collisions(float offset) {
        if (!ontopOfCollidablePoly()) {
            yBottom = 0;
        }
        for (Quad v : world.quad) {
            if (Math.abs(vector.x - v.centerX) < (v.width / 2) + 0.2f
                    && Math.abs(vector.z - v.centerZ) < (v.length / 2) + 0.2f) {
                if ((Math.abs(vector.y - v.y2) < v.height + (offset))
                        || vector.y > v.yBottom + v.height - 0.5f) {
                    if (vector.y > v.y2 - 0.01f) {
                        yBottom = v.y1 + v.yTop;
                    } else {
                        if (Math.abs(vector.x - v.centerX) < (v.width / 2) + 0.2f) {
                            vector.x = previous.x;
                        }
                        if (Math.abs(vector.z - v.centerZ) < (v.length / 2) + 0.2f) {
                            vector.z = previous.z;
                        }
                    }
                } else if (v.yBottom > vector.y + 2.4f) {
                    if (vector.y + 3 > v.y1) {
                        yMotion = 0;
                        vector.y = v.y2 - 3;
                    }
                } else {
                    vector.x = previous.x;
                    vector.y = previous.y;
                    vector.z = previous.z;
                }
            }
        }
    }

    private boolean ontopOfCollidablePoly() {
        for (Quad v : world.quad) {
            if (Math.abs(vector.x - v.centerX) < v.width / 2
                    && Math.abs(vector.z - v.centerZ) < v.length / 2) {
                return true;
            }
        }
        return false;
    }

    // управление
    private void Input() {
        if (Main.keys[Keyboard.KEY_W]) {
            moveForward = true;
        } else {
            moveForward = false;
        }
        if (Main.keys[Keyboard.KEY_S]) {
            moveBackward = true;
        } else {
            moveBackward = false;
        }
        if (Main.keys[Keyboard.KEY_A]) {
            strafeLeft = true;
        } else {
            strafeLeft = false;
        }
        if (Main.keys[Keyboard.KEY_D]) {
            strafeRight = true;
        } else {
            strafeRight = false;
        }
        if (Main.keys[Keyboard.KEY_SPACE] && onGround) {
            yMotion = 0.2f;
        }
        // если курсор исчез...
        if (Mouse.isGrabbed()) {
            // произойдет FIX камеры
            float mouseDX = Mouse.getDX() * 0.8f * 0.16f;
            float mouseDY = Mouse.getDY() * 0.8f * 0.16f;
            if (rotation.y + mouseDX >= 360) {
                rotation.y = rotation.y + mouseDX - 360;
            } else if (rotation.y + mouseDX < 0) {
                rotation.y = 360 - rotation.y + mouseDX;
            } else {
                rotation.y += mouseDX;
            }
            if (rotation.x - mouseDY >= -89 && rotation.x - mouseDY <= 89) {
                rotation.x += -mouseDY;
            } else if (rotation.x - mouseDY < -89) {
                rotation.x = -89;
            } else if (rotation.x - mouseDY > 89) {
                rotation.x = 89;
            }
        }
    }

    private void UpdateMotion(float distance) {
        if (moveForward) {
            vector.x += distance * (float) Math.sin(rotation.y * Math.PI / 180);
            vector.z += -distance
                    * (float) Math.cos(rotation.y * Math.PI / 180);
        }
        if (moveBackward) {
            vector.x -= distance * (float) Math.sin(rotation.y * Math.PI / 180);
            vector.z -= -distance
                    * (float) Math.cos(rotation.y * Math.PI / 180);
        }
        if (strafeLeft) {
            vector.x += distance
                    * (float) Math.sin((rotation.y - 90) * Math.PI / 180);
            vector.z += -distance
                    * (float) Math.cos((rotation.y - 90) * Math.PI / 180);
        }
        if (strafeRight) {
            vector.x += distance
                    * (float) Math.sin((rotation.y + 90) * Math.PI / 180);
            vector.z += -distance
                    * (float) Math.cos((rotation.y + 90) * Math.PI / 180);
        }
    }
}